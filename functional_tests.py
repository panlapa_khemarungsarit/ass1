from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import unittest


class NewVisitorTest(unittest.TestCase):

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def check_row(self,row_text):
        table = self.browser.find_element_by_id('id_list_table')
        rows = table.find_elements_by_tag_name('td')
        self.assertIn(row_text, [row.text for row in rows])

    def test_send_data(self):
        self.browser.get('http://localhost:8000')
        link_save_book = self.browser.find_element_by_id('save book')
        self.assertEqual(
                link_save_book.get_attribute('value'),
                'save book'
        )
        # actions = ActionChains(self)
        link_save_book.click()
        self.assertIn('save book', self.browser.title)
        inputbox_name = self.browser.find_element_by_id('name_new_book')
        inputbox_num = self.browser.find_element_by_id('num_new_book')
        inputbox_cost = self.browser.find_element_by_id('cost_new_book')
        inputbox_comment = self.browser.find_element_by_id('comment_new_book')
        inputbox_borrow = self.browser.find_element_by_id('borrow_new_book1')
        inputbox_name.send_keys('book1')
        inputbox_num.send_keys('1')
        inputbox_cost.send_keys('100')
        inputbox_comment.send_keys('comment1')
        inputbox_borrow.click()
        button = self.browser.find_element_by_id('button')
        button.click()
        self.check_row('book1')
        self.fail('Finish the test!')


if __name__ == '__main__':
    unittest.main(warnings='ignore')
