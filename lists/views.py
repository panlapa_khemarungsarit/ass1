#!/usr/bin/python
from django.shortcuts import redirect, render
from lists.models import book
from lists.models import count_cost_book
import operator

def home_page(request):
    if (request.method == 'POST' and request.POST.get('delete','') == 'delete'):
        id_book = request.POST['id_delete']
        book.objects.get(pk = id_book).delete()
        return redirect('/')
    books = book.objects.all()
    list_book = []
    list_book_row = []
    for b in books:
        list_book.append(b.name)
        list_book.append(b.num)
        list_book.append(b.cost)
        list_book.append(b.borrow)
        list_book.append(b.comment)
        list_book.append(b.id)   
        list_book_row.append(list_book)
        list_book = []
    if(len(list_book_row) == 0):
         return render(request, 'home.html', {'books': []})
    list_book_row = sorted(list_book_row,key=operator.itemgetter(0,1))
    return render(request, 'home.html', {'books': list_book_row})

def save_book(request):
    if request.method == 'POST' and request.POST.get('black','') == 'black':
        return redirect('/')
    if request.method == 'POST' and request.POST.get('send','') == 'send':
        name_book_text = request.POST['name_book']
        num_book_text = request.POST['num_book']
        cost_book_text = request.POST['cost_book']
        comment_book_text = request.POST['comment_book']
        if( request.POST.get('borrow_book','') == 'Y'):
            borrow_book_text = "Y"
        else:
            borrow_book_text = "N"
        books_for_check = book.objects.all()
        check = 0;
        for b_c in books_for_check :
            if(b_c.name == name_book_text):
                if(b_c.num == num_book_text):
                    check = 1
        if check == 1:
             return render(request, 'save_book.html', {'books': [],'text':"this book have in table"})
        else:
            book.objects.create(
                name = name_book_text,
                num = num_book_text,
                cost = cost_book_text,
                comment = comment_book_text,
                borrow  = borrow_book_text
                )
            return redirect('/save_book')
    books = book.objects.all()
    list_book = []
    list_book_row = []
    for b in books:
        list_book.append(b.name)
        list_book.append(b.num)
        list_book.append(b.cost)
        list_book.append(b.borrow)
        list_book.append(b.comment)
        list_book_row.append(list_book)
        list_book = []

    if(len(list_book_row) == 0):
         return render(request, 'save_book.html', {'books': [], 'text' : ""})

    list_book_row = sorted(list_book_row,key=operator.itemgetter(0,1))
    return render(request, 'save_book.html', {'books': list_book_row , 'text' : ""})

def count_book(request):
    if request.method == 'POST' and request.POST.get('black','') == 'black':
        return redirect('/')
    if (request.method == 'POST' and request.POST.get('button_send','') == 'send'):
        name_book_text = request.POST['name_book']
        num_book_text = request.POST['num_book']
        cost_book_text = request.POST['cost_book']
        count_cost_book.objects.create(
                name = name_book_text,
                num = num_book_text,
                cost = cost_book_text,
                )
        return redirect('/count')
    elif(request.method == 'POST' and request.POST.get('delete','') == 'delete'):
        id_book = request.POST['id_delete']
        count_cost_book.objects.get(pk = id_book).delete()
        return redirect('/count')
    books = book.objects.all()
    books_count = count_cost_book.objects.all()
    list_book = []
    list_book_row = []
    count = 0;
    for b in books_count:
        list_book.append(b.name)
        list_book.append(b.num)
        count = count + int(b.cost)
        list_book.append(b.cost)
        list_book.append(b.id)
        check = 0
        for b_c in books :
            if(b_c.name == b.name):
                if(b_c.num == b.num):
                    check = 1
        if(check == 1):
            list_book.append("this book have in table")
        else:
            list_book.append("")
        list_book_row.append(list_book)
        list_book = []
    if(len(list_book_row) == 0):
         return render(request, 'cost_book.html', {'books': [], 'count_cost' : 0})
    return render(request, 'cost_book.html', {'books': list_book_row,'count_cost' : count})

def serch_book(request):
    if request.method == 'POST' and request.POST.get('black','') == 'black':
        return redirect('/')
    if(request.method == 'POST' and request.POST.get('delete','') == 'delete'):
        id_book = request.POST['id_delete']
        book.objects.get(pk = id_book).delete()
        return redirect('/serch')
    elif (request.method == 'POST' and request.POST.get('b_send','') == 'send'):
        books = book.objects.all()
        list_book = []
        list_book_row = []
        for b in books:
            if request.POST['name_book'] in b.name:
                list_book.append(b.name)
                list_book.append(b.num)
                list_book.append(b.cost)
                list_book.append(b.borrow)
                list_book.append(b.comment)
                list_book.append(b.id)
                list_book_row.append(list_book)
                list_book = []
        if(len(list_book_row) == 0):
            return render(request, 'serch.html', {'books': [], 'text' : "no data"})
        return render(request, 'serch.html', {'books': list_book_row, 'text' : ""})
    return render(request, 'serch.html', {'books': [], 'text' : ""})

def borrow_book(request):
    if request.method == 'POST' and request.POST.get('black','') == 'black':
        return redirect('/')
    books = book.objects.all()
    list_book = []
    list_book_row = []
    for b in books:
        if b.borrow == "Y":
            list_book.append(b.name)
            list_book.append(b.num)
            list_book.append(b.cost)
            list_book.append(b.borrow)
            list_book.append(b.comment)
            list_book_row.append(list_book)
            list_book = []
    if(len(list_book_row) == 0):
       return render(request, 'borrow_book.html', {'books': [], 'text' : "no bowwor"})
    return render(request, 'borrow_book.html', {'books': list_book_row, 'text' : ""})
