from django.core.urlresolvers import resolve
from django.test import TestCase
from django.http import HttpRequest
from django.template.loader import render_to_string
from lists.views import home_page
from lists.views import save_book
from lists.models import book


class HomePageTest(TestCase):

    def test_returns_correct_html(self):
        request = HttpRequest()
        response = save_book(request)
        expected_html = render_to_string('save_book.html')
        self.assertEqual(response.content.decode(), expected_html)

    def test_displays_all_list_items(self):
        book.objects.create(name = 'book 1',num = '1',cost = '100',comment = 'comment1')
        book.objects.create(name = 'book 2',num = '2',cost = '200',comment = 'comment2')
        # book.objects.create(name = 'book 1')
        self.assertEqual(book.objects.count(), 2)
        request = HttpRequest()
        response = save_book(request)
        self.assertIn('book 1', response.content.decode())
        self.assertIn('1', response.content.decode())
        self.assertIn('100', response.content.decode())
        self.assertIn('comment1', response.content.decode())

    def test_post(self):
        request = HttpRequest()
        request.method = 'POST'
        request.POST['name_book'] = 'A new name'
        request.POST['num_book'] = '1'
        request.POST['cost_book'] = '100'
        request.POST['comment_book'] = 'A new comment'
        response = save_book(request)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/save_book')
