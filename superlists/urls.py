from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^$', 'lists.views.home_page', name='home'),
    url(r'save_book/$', 'lists.views.save_book'),
    url(r'count/$', 'lists.views.count_book'),
    url(r'serch/$', 'lists.views.serch_book'),
    url(r'borrow/$', 'lists.views.borrow_book'),
    # url(r'edit/$', 'lists.views.edit_book'),
    # Examples:
    # url(r'^$', 'superlists.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
)
